Quattro - Fluentd Docker Image
==============================

This image contains all necessary plugins needed for the Quattro environment.

Usage see: https://github.com/fluent/fluentd-docker-image/blob/master/README.md
